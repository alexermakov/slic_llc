$(document).ready(function () {


    if ($('.js__map').length) {

        let map;
        let marker;
        let location = [];
        let $map = $('.js__map')[0];



        $('.js__map').each(function () {
            let coord = $(this).attr('data-coord').split(', ')
            location.lat = parseFloat(coord[0]),
            location.lng = parseFloat(coord[1]);
        })

        marker = new google.maps.Marker({
            position: {
                lat: location.lat,
                lng: location.lng
            },
        });


        function gmapRender() {
            map = new google.maps.Map($map, {
                center: {
                    lat: location.lat,
                    lng: location.lng
                },
                zoom: 16,
                // mapTypeControl: false,
                // zoomControl: false,
                // scaleControl: false,
                navigationControl: false,
                streetViewControl: true,
            });
            marker.setMap(map);
        }


        gmapRender()


    }



    if ($('.js__map__contact').length) {


        function gmap() {

            let map;
            let locations = [];

            let $map = $('.js__map__contact')[0];

            $('.js__map__contact').each(function () {
                let coord = $(this).attr('data-coord').split(', ')
                locations.push({
                    lat: parseFloat(coord[0]),
                    lng: parseFloat(coord[1]),
                })
            })

            map = new google.maps.Map($map, {
                center: {
                    lat: locations[0].lat,
                    lng: locations[0].lng
                },
                zoom: 10,
                styles: gmapStyle,
                navigationControl: false,
                streetViewControl: false,
            });


            const markers = locations.map((item, i) => {
                const marker = new google.maps.Marker({
                    position: {
                        lat: item.lat,
                        lng: item.lng
                    },
                });

                return marker;
            });


        }

        gmap()
    }
});