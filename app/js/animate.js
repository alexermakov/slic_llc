$(document).ready(function () {
    function isInView(elem){
        return (elem.offset().top - $(window).height() / 2 < $(window).scrollTop()) && ((elem.offset().top + elem.outerHeight()) -  $(window).height() / 2> ($(window).scrollTop()));
    }
    function isInViewStep(elem){
        let centerScreen = $(window).scrollTop() + $(window).height() / 2 ;
        let middle = elem.offset().top + elem.outerHeight() / 2

        console.log('centerScreen '+centerScreen)
        console.log('middle '+middle)
        indexStep = centerScreen > middle ? 1 : 0;
        return indexStep;
    }




    $(window).resize(function () {
        doSticy()

    });

    function doSticy(){
        if ($(window).width()<=800){
            Stickyfill.removeAll()
            $('.js_home_sticky_content__block').removeAttr('style')
        }else{
            let heightStickyBlock = 0;
            $('.js_home_sticky_title__step').each(function (index, element) {
               heightStickyBlock +=$(this).outerHeight()

            });
            $('.js_home_sticky_content__block').height(heightStickyBlock)

            Stickyfill.forceSticky()
            let elements = $('.js_home_sticky_title__block');
            Stickyfill.add(elements);

            let elementsx = $('.js_home_sticky_title__block_2');
            Stickyfill.add(elementsx);
        }
    }

    doSticy()





    $(window).scroll(function(){
        if ($('.js_home_sticky_title__block').length){
            if (isInView($('.js_home_sticky_section'))){
                let indexStep = isInViewStep($('.js_home_sticky_content__block'))

                console.log(indexStep)

                if (!$('.js_home_sticky_title__block .home_sticky_title').eq(indexStep).hasClass('active')){
                    $('.js_home_sticky_title__block .home_sticky_title.active').removeClass('active')
                    $('.js_home_sticky_title__block .home_sticky_title').eq(indexStep).addClass('active');
                    $('.js_home_sticky_title__step.active').removeClass('active');
                    $('.js_home_sticky_title__step').eq(indexStep).addClass('active');
                }
            }
        }
     })


    function startScreenAnimate(){

        $(".__animate_promo_curtain").each(function () {

            let curtainMove = $(window).width()>1000 ? '15rem': '22.5%';
            el = $(this)
            elParent = $('.__animate_home_promo')[0];

            gsap.to(el, {
                y: curtainMove,
                ease: "cubic-bezier(.19,1,.22,1)",
                scrollTrigger: {
                    trigger: elParent,
                    start: "top 9.5%",
                    end: "bottom top",
                    scrub:true
                }
            })
        })


        $(".__animate__home_about__title__decore__text").each(function () {

            el = $(this)
            elParent = $('.__animate__home_about__title__decore_parent')[0];

            gsap.to(el, {
                x: '-30rem',
                ease: "cubic-bezier(.19,1,.22,1)",
                scrollTrigger: {
                    trigger: elParent,
                    start: "top 9.5%",
                    end: "bottom top",
                    scrub: 1
                }
            })
        })





        $(".__animate_decore__home__line").each(function () {
            el = $(this)
            elParent = $(this).parent()[0];

            gsap.to(el, {
                scrollTrigger: {
                    trigger: elParent,
                    start: 'top 85%',
                },
                bottom: 0,
                ease: "ease-in-out",
                duration: 10

            })
        })

        setTimeout(() => {
            setTimeout(() => {
                $(".__animate_header").each(function () {
                    el = $(this)
                    elParent = $(this).parent()[0];

                    gsap.to(el, {
                        scrollTrigger: {
                            trigger: elParent,
                            start: 'top 85%',
                        },
                        y: 0,
                        opacity:1,
                        ease: "ease-in-out",
                        duration: 1

                    })
                })
            }, 100)

            setTimeout(() => {
                $(".__animate__top__start").each(function () {
                    el = $(this)
                    elParent = $(this).parent()[0];

                    gsap.to(el, {
                        scrollTrigger: {
                            trigger: elParent,
                            start: 'top 85%',
                        },
                        y: '0',
                        opacity:1,
                        ease: "ease-in-out",
                        duration: 1

                    })
                })
            }, 0)

            setTimeout(() => {
                $(".__animate__start_opacity").each(function () {
                    el = $(this)
                    elParent = $(this).parent()[0];

                    gsap.to(el, {
                        scrollTrigger: {
                            trigger: elParent,
                            start: 'top 85%',
                        },
                        opacity:1,
                        ease: "ease-in-out",
                        duration: 1

                    })
                })
            },100)

            setTimeout(() => {

                $(".__title_animate__top__start").each(function () {
                    el = $(this).find('span>span')
                    elParent = $(this)[0];

                    gsap.to(el, {
                        scrollTrigger: {
                            trigger: elParent,
                            start: 'top 95%',
                        },
                        y: '0',
                        ease: "ease-in-out",
                        stagger: {
                            amount: 0.3
                        },
                        duration: 1

                    })
                })
            }, 0)

            setTimeout(() => {

                $(".__animate__start_scale_down").each(function () {
                    el = $(this);
                    elParent = $(this)[0];

                    gsap.to(el, {
                        scrollTrigger: {
                            trigger: elParent,
                            start: 'top 95%',
                        },
                        scale: 1,
                        ease: "ease-in-out",
                        stagger: {
                            amount: 0.3
                        },
                        duration: 1

                    })
                })
            }, 0)
        },250)








//         __title_animate__top
// __animate__top__start
    }


    $(".__animate__top").each(function () {
        el = $(this)
        elParent = $(this).parent()[0];

        gsap.to(el, {
            scrollTrigger: {
                trigger: elParent,
                start: 'top 95%',
            },
            y: '0',
            opacity:1,
            ease: "ease-in-out",
            duration: 1

        })
    })

    $(".__animate__opacity").each(function () {
        el = $(this)
        elParent = $(this).parent()[0];

        gsap.to(el, {
            scrollTrigger: {
                trigger: elParent,
                start: 'top 90%',
            },
            y: '0',
            opacity:1,
            ease: "ease-in-out",
            duration: 1

        })
    })


    $(".__animate__scale_down").each(function () {
        el = $(this);
        elParent = $(this)[0];

        gsap.to(el, {
            scrollTrigger: {
                trigger: elParent,
                start: 'top 95%',
            },
            scale: 1,
            ease: "ease-in-out",
            stagger: {
                amount: 0.3
            },
            duration: 1

        })
    })


    $(".__animate__line_home_about").each(function () {
        el = $(this);
        elParent = $(this)[0];

        gsap.to(el, {
            scrollTrigger: {
                trigger: elParent,
                start: 'top 95%',
            },
            scale: 1,
            ease: "ease-in-out",
            stagger: {
                amount: 0.3
            },
            duration: 1

        })
    })

    $(".__animate__footer_line").each(function () {
        el = $(this);
        elParent = $(this)[0];

        gsap.to(el, {
            scrollTrigger: {
                trigger: elParent,
                start: 'top 95%',
            },
            y: 0,
            ease: "ease-in-out",
            stagger: {
                amount: 0.3
            },
            duration: 1

        })
    })

    $(".__animate__case__move_top").each(function () {
        el = $(this);
        elParent = $(this)[0];

        gsap.to(el, {
            scrollTrigger: {
                trigger: elParent,
                start: 'top 95%',
            },
            height: '100%',
            ease: "ease-in-out",
            stagger: {
                amount: 0.3
            },
            duration: 1

        })
    })
    if ($('body').hasClass('home')){
        $('.js_loader_x').addClass('visible')
        loadedPreloader()
    }

    function loadedPreloader() {
        const allImages = document.querySelectorAll('img');
        const allImagesCount = allImages.length;
        let countImageLoaded = 0;

        function removeLoader() {
            setTimeout(() => {
                $('.js_loader_x').addClass('showed')
                startScreenAnimate()
            }, 100);



        }

        const load = url => new Promise(resolve => {
            const img = new Image()
            img.onload = () => resolve({
                url
            })
            img.src = url;
            countImageLoaded++;
        });




        allImages.forEach(image => {
            (async () => {
                const {
                    url
                } = await load(image.src);
            })();
            if (allImagesCount == countImageLoaded) {
                setTimeout(() => {
                    setTimeout(() => {
                        $('.js_loader_x').addClass('showed_forlogo')
                    }, 500)

                    setTimeout(() => {
                        removeLoader();
                    }, 750);
                }, 500)

            }
        });
    }






});