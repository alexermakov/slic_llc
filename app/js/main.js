$(function () {

    function isInView(elem){
        return (elem.offset().top - $(window).height() / 2 < $(window).scrollTop()) && ((elem.offset().top + elem.outerHeight()) -  $(window).height() / 2> ($(window).scrollTop()));
    }

    $(window).on("load", function () {
        $('body').addClass('loaded')
    })

    Fancybox.bind('.js__modal', {
        autoFocus: false,
        trapFocus: false,
        touch: false,
        closeButton: 'outside',
    });


    $(".js_select").each(function () {
        let placeholder = $(this).attr('placeholder');
        $(this).select2({
            minimumResultsForSearch: 1 / 0,
            placeholder: placeholder,
            // allowClear: true
        })
    });


    $('.js__btn_close__modal').click(function (e) {
        Fancybox.close();
    });


    $('.js__btn_open__modal').click(function (e) {
        Fancybox.show([{
            src: "#js_modal__call"
        }])
    });


    $('.js_btn_menu,.js_modal__menu__btn_close,.js_modal__menu_overlay').click(function (e) {
        e.preventDefault();
        $('.js_btn_menu').toggleClass('active')
        $('.js__modal__menu').toggleClass('active')
        $('.js_modal__menu_overlay').toggleClass('active')
        $('body').toggleClass('body-lock')
    });



    $('.js_single_portfolio_slider').slick({
        dots: false,
        arrows:true,
        infinite: true,
        speed: 700,
        cssEase: 'linear'
    })

    $('.js_home_aquisitions__slider').slick({
        dots: false,
        arrows:true,
        infinite: true,
        variableWidth: true,
        swipeToSlide:true,
        speed: 700,
        touchThreshold:10,
        cssEase: 'linear',
        prevArrow: $('.js_home_aquisitions__arrows>*:first-child'),
        nextArrow: $('.js_home_aquisitions__arrows>*:last-child')
    })





    $('.js_form').submit(function (event) {
        let $form =  $(this);
        if ($(this)[0].checkValidity()) {
            let formData = $(this).serialize();
            // $.ajax({
            //     type: "POST",
            //     url: "sendmail.php",
            //     data: formData,
            //     success: function (msg) {
            //          do something
            //     }
            // });

            if ($form.closest('.js_contact__main__form').length){
                $('.js_contact__main__form__thanks').fadeIn(400);
                $('.js_contact__main__form__content').fadeOut(400)
            }
        }
    });

});